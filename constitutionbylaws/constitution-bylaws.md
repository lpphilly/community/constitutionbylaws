% LPPhilly Constitution and Bylaws
% 2018-02-20

# LIBERTARIAN PARTY OF PHILADELPHIA

**Commonwealth of Pennsylvania**

## CONSTITUTION

### ARTICLE I -- NAME

This organization shall be known as the Libertarian Party of Philadelphia, hereinafter referred to LP_PH.

### ARTICLE II -- PURPOSE

The purpose of LP_PH is to help elect and re-elect candidates who will implement Libertarian policies, principally in Philadelphia.

### ARTICLE III -- MEMBERSHIP

Any person who endorses the purposes and principles of the Libertarian Party may become a member subject to the provisions of the Constitution and Bylaws.

### ARTICLE IV -- ORGANIZATION

-   Section 1 -- OFFICERS

The officers of LP_PH shall be Chair, Vice Chair, Secretary, Treasurer and LPPA Board Representative. All of these shall be elected by the membership present at the Annual Meeting of LP_PH (see Article VI) and shall take office immediately upon the close of that meeting.

-   Section 2 -- EXECUTIVE COMMITTEE

The Executive Committee shall consist of the elected officers, the chair of any standing committee and one or two member-at-large elected by the membership.

-   Section 3 -- APPEALS COMMITTEE

The Appeals Committee shall be the final body of appeal in all material matters concerning interpretations of the Constitution and Bylaws as defined in the Bylaws.

-   Section 4 -- QUALIFICATIONS

Each member of the Executive Committee and the Appeals Committee shall be required to maintain a membership in good standing with LP_PH. Any member ceasing to meet this requirement shall not be eligible to vote on any matter that shall come before the body of which they're a member.

### ARTICLE V -- ANNUAL MEETING

LP_PH shall hold an Annual Meeting to conduct such business as may properly come before it at a time and place to be set according to the Bylaws and in compliance with the Constitution and Bylaws. This meeting shall occur prior to the date of the Primary Election of the Commonwealth of Pennsylvania and prior to the Annual Convention of the Libertarian Party of Pennsylvania.

### ARTICLE VI -- BYLAWS

The Bylaws are subordinate to this Constitution.

### ARTICLE VII -- AMENDMENTS

This Constitution may be amended by a two-thirds vote of all members present at the Annual Meeting. Amendments and/or notices of amendment must be submitted in writing at least 10 days prior to the Annual Meeting to the Secretary.

## LIBERTARIAN PARTY OF PHILADELPHIA BYLAWS

### ARTICLE I -- MEMBERSHIP

-   Section 1 -- Classes of Membership

To establish and maintain membership in good standing, all must complete an Application for Membership Form and pay such dues as are required by the Executive Committee.

-   *VOTING MEMBER*

A voting member shall have the right to participate and vote on all matters brought before the assembly of members at meetings of LP_PH and to hold any office or chair any committee in the organization. A voting member must have previously attended three meetings in a six month period, have a stake in Philadelphia for work or as either a resident or full time student.

-   ASSOCIATE MEMBER

An associate member may be anyone interested in the Libertarian Party who has attended two meetings in a six month period. They shall have the right to attend and participate in assemblies of the general membership but may not vote on any matter brought before the assembly nor hold any office nor chair any committee. Associate members and first-time attendees may vote if, whether separately or together, a vote is called upon by the voting members in attendance to waive this requirement for votes for those remaining in attendance who do not meet these requirements. A simple majority will be required to waive these restrictions to allow the vote. This waive takes effect for that meeting only but can be voted on again at future meetings.

-   STUDENT MEMBER

A student member must be a student who endorses the purposes of LP_PH and meet the requirements of a voting member. A student member shall have all the rights and benefits of a voting member and shall pay one-half the dues established by the Executive Committee for a voting member.

-   Section 2 -- Dues

The Executive Committee from time to time shall determine the amount of dues necessary for the membership classes.

-   Section 3 -- Meetings

The Chair or acting chair shall determine the time and place for all meetings. A place can be a physical location, virtual or both. The Secretary will help to notify the members at least 7 days in advance of the meeting except for emergency or urgent meetings. The Chair or acting chair will preside at all meetings, will prepare an agenda, enforce rules of order and, where necessary, invoke a time limit for speakers. Motions and amendments, passed or failed, must be given to the Secretary. A quorum for membership meetings shall be equal to the number of officers plus one (six total if all standard officer positions are occupied).

-   Section 4 -- Suspension of Membership

The Executive Committee shall have the power to suspend a member for failure to maintain all qualifications of membership or for cause by a two-thirds vote of the Committee.

**A member may be suspended by the Committee for cause for:**

a) unreasonable behavior in meetings (e.g. by repeatedly interrupting others or being loud or rude or negative)

b) misrepresenting LP_PH

c) endorsing or campaigning in the name of LP_PH purporting to have been nominated or endorsed by LP_PH without having received such nomination or endorsement

d) abuse of power

e) for other reasonable cause

Once suspended, a member must immediately leave a meeting.

Notification of suspension shall be in writing and is subject to written appeal as described in Article I Section 5 within fifteen days of notification. Failure to appeal shall result in termination of membership.

-   Section 5 -- Appeal from a Suspended Member

Upon appeal by a suspended member, the Appeals Committee shall hold a hearing within 15 days concerning the suspension. Following the hearing, the Appeals Committee shall rule either to terminate the member or to continue membership in good standing. If the Committee fails to rule, the original decision stands.

### ARTICLE II -- OFFICERS

All officers shall be elected by majority vote of membership present at the Annual Meeting. If LP_PH is being restarted after a period of inactivity, officers may be appointed in a manner that is reasonable in all the circumstances. Duties of all officers as established by these bylaws may be augmented by resolution at any regular meeting of the membership. All officers are required to maintain membership in good standing with LP_PH.

-   Section 1 -- Chair

The Chair shall preside at all membership meetings of LP_PH and at all meetings of the Executive Committee, act as chief executive officer of LP_PH and arrange the place and time for meetings. The Chair shall make committee chair appointments that must be approved by the Executive Committee. At any meeting, the Chair may invoke a time limit for the meeting or for any subject matter being considered at any meeting. If the Chair and Vice-Chair are unable to attend a meeting, the chair shall appoint another executive committee member to run that meeting.

-   Section 2 -- Vice Chair

The Vice Chair shall act as assistant to the Chair, coordinate all party candidates and activities, act as Chair in the absence of the Chair and assist the Chair in preparations for meetings.

-   Section 3 -- Secretary

The Secretary shall record and maintain in good order the minutes of all meetings of the membership and of the Executive Committee, notify members of meetings and maintain a list of members, and file all required non-financial reports on behalf of LP_PH with government agencies, and parent affiliate.

-   Section 4 -- Treasurer

The Treasurer shall receive, disburse and account for the funds of LP_PH under the supervision of the Chair and the Executive Committee, compile a quarterly report consisting of a balance sheet and a profit and loss statement which shall be available to the officers and members upon request, file all required financial reports on behalf of LP_PH with government agencies and cooperate with the Financial Committee in budget planning.

-   Section 5 -- LPPA Board Representative

The LPPA Board Representative shall attend all board meetings and conferences of the Libertarian Party of Pennsylvania to represent the LP_PH. If the Board Representative is unable to attend one of these meetings, the Board Representative must appoint a proxy member from the LP_PH to attend in their stead

-   Section 6 -- Suspension of an Officer

An officer or a member at large may be suspended from office by a two-thirds vote of the Executive Committee for reasons described in Article I Section 4, for failure to fulfill the duties of the office held or for other reasonable cause. The office of a suspended officer or member at large shall be declared vacant unless the suspended officer appeals the suspension to the Appeals Committee within 10 days of notification of suspension. A suspended officer must immediately leave the meeting.

-   Section 7 -- Recall Provision

Any member present and eligible to vote at the last annual meeting (Eligible Voting Members) may submit a petition to recall any specific officer, member at large, or appeals committee member of the LP_PH to the Secretary. If the Secretary is to be recalled or if the Secretary position is vacant, the petition may alternatively be sent to the Chair. Petitions must be signed by at least two other Eligible Voting Members and submitted at least 10 days in advance of a regular meeting that's not an annual meeting. The request shall be read at the next regular meeting and the previously notified officer must provide notice to all Eligible Voting Members within five days. The recall shall be voted on at a subsequent regular meeting to be held within 10 to 40 days, at which time the recall passes only if 2/3 of all those from the last annual meeting vote in favor of the recall. If the recall passes, the seat in question is considered vacant.

-   Section 8 -- Appeals from a Suspended Officer

Upon written appeal by a suspended officer or member at large, the Appeals Committee shall set the date of a hearing. Following the hearing, the Appeals Committee shall rule within three days to either uphold the suspension, thereby vacating the office, or to restore the officer to full authority. Should the Appeals Committee fail to rule, the original decision stands.

-   Section 9 -- Vacancies of Office

All recommendations to fill vacancies or appointments to new working committees shall be made by the Chair with the approval by a majority vote of the Executive Committee. It is the responsibility of the Executive Committee to appoint new officers or members at large if vacancies or suspensions occur, such officers to complete the term of office vacated. The Executive Committee may, at its discretion and upon majority vote with a quorum present, leave an officer or member at large position, vacated for above-mentioned cause, vacant until the term of office has elapsed.

### ARTICLE III -- COMMITTEES

-   Section 1 - APPEALS COMMITTEE

The Appeals Committee shall be composed of three (3) LP_PH voting members. The Appeals Committee shall be the final body of appeal in all material matters concerning interpretations of these Bylaws. The Appeals Committee shall be the final body for appeals of actions taken by the Committee and Executive Committee.  Members of the Appeals Committee shall be elected by a majority of the voting members present at the LP_PH Annual Meeting and shall take office immediately upon the close of that LP_PH Annual Meeting and serve thereafter until final adjournment of the next LP_PH Annual Meeting. No member of the Appeals Committee may serve in any capacity on the Executive Committee.

-   Section 2 - STANDING COMMITTEES 

The standing committees of the Committee may be established at Annual Meetings to address long-term issues of interest to the LP_PH. The duties, composition and reporting requirements shall be determined from time to time by the Executive Committee.

-   Section 3 - WORKING COMMITTEES

The LP_PH may from time-to-time establish Working Committees to address specific, temporary issues of interest to the LP_PH. The LP_PH shall have full authority to create and disband any Working Committee at any time, as need dictates. The Chair of a Working Committee shall not be considered a member of the Executive Committee, nor have a vote on the Board.

-   Section 4 - QUALIFICATIONS

Each member of the Executive Committee, the Appeals Committee, and any other Committee authorized by these Bylaws shall be required to maintain membership in good standing with LP_PH. Any member ceasing to meet this requirement shall not be eligible to vote on any matter that shall come before the body to which they are elected until membership in good standing is restored.

-   Section 5 - DUTIES

  -   The duties, composition and reporting requirements of each committee shall be codified in a Committees Manual formulated by the Executive Committee and maintained by the Secretary which shall be subservient to these Bylaws in all cases. The manuals can be changed at any regular meeting of the LP_PH by a majority vote of the Executive Committee.
  -   The Executive Committee shall be empowered to remove, by unanimous vote, any member of any Standing or Working committee found to be in breach of the duties ascribed to them in the Committees Manual. The Executive Committee is not granted power to remove members of the Appeals Committee unless full membership is revoked subject to Article III, Section 5. 

### ARTICLE IV -- EXECUTIVE COMMITTEE

The Executive Committee shall consist of the officers of LP_PH, the chair of any standing committee and may include one or two members-at-large elected by the membership at the Annual Meeting. The members-at large must be a voting member and must not concurrently hold any office or chair any committee. A quorum for the Executive Committee shall be a majority of the Committee.

The Executive Committee shall be responsible for the control and management of all the affairs, properties and funds of LP_PH consistent with the Constitution, Bylaws and any resolutions that may be adopted. Each member of the Executive committee shall be required to maintain membership in good standing with LP_PH.

-   Section 1 -- Meeting Notification

The Executive Committee shall meet at such time and place as may be determined by a call of the Chair or by the request of one third or more of the members of the Executive Committee. Notice of the time and place of all meetings shall be given to each member of the Committee not less than fourteen days prior to said meeting unless an earlier date is agreed upon by a majority of the Committee. At least one meeting must be held during each calendar quarter. Urgent or emergency meetings may be requested by any member of the Committee and the Chair, at their discretion, may call for such a meeting to be held.

Section 2 -- Public Meetings

The Executive Committee may hold public meetings during the year, which shall be open to all registered Libertarian Party voters. Members in good standing of LP_PH shall be notified of these meetings no less than fourteen days prior to the meeting.

### ARTICLE V -- NOMINATIONS OF CANDIDATES FOR OFFICE

Candidates for local office shall be nominated by the Executive Committee. The candidate shall then be approved by a majority vote of voting members present during the meeting.

### ARTICLE VI -- PARLIAMENTARY AUTHORITY

Robert's Rules of Order as newly revised shall be the parliamentary authority for all matters of procedure not specifically covered in these Bylaws.

### ARTICLE VII -- AMENDMENTS

These Bylaws may be amended by a two-thirds vote of all voting members at the Annual Meeting of LP_PH. All amendments or notices of amendments shall be submitted in writing to the Secretary at least 10 days prior to the Annual Meeting.
