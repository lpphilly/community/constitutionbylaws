# Constitution - Bylaws
***Libertarian Party of Delaware Philadelphia***

## Description
Includes a simple build script to convert all markdown files to other common document formats.
+ pdf
+ epub

## Usage
Edit markdown files and run build.sh

## Core Dependencies
pandoc
